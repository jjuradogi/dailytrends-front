import { TestBed, async } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CardModule } from 'primeng/card';
import { FeedCardComponent } from './feed-card.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('FeedCardComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, CommonModule, RouterModule, CardModule, RouterTestingModule],
      declarations: [FeedCardComponent],
    }).compileComponents();
  }));

  it('should create the feed-card', () => {
    const fixture = TestBed.createComponent(FeedCardComponent);
    const feedCard = fixture.debugElement.componentInstance;

    expect(feedCard).toBeTruthy();
  });
});
