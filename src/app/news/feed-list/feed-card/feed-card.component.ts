import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

import { Feed } from '../../feed.model';

@Component({
  selector: 'app-feed-card',
  templateUrl: './feed-card.component.html',
  styleUrls: ['./feed-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FeedCardComponent implements OnInit {
  @Input()
  feed: Feed;

  constructor() {}

  ngOnInit() {}
}
