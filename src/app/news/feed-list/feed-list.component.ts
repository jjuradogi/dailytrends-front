import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { FeedService } from '../../shared/services/feed.service';
import { Feed } from '../feed.model';

@Component({
  selector: 'app-feed-list',
  templateUrl: './feed-list.component.html',
  styleUrls: ['./feed-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class FeedListComponent implements OnInit {
  feeds: Feed[];

  constructor(private feedService: FeedService) {}

  ngOnInit() {
    this.getFeeds();
  }

  getFeeds(): void {
    this.feedService.getFeeds().subscribe(feeds => (this.feeds = feeds));
  }
}
