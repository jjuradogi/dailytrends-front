import { TestBed, async } from '@angular/core/testing';
import { FeedListComponent } from './feed-list.component';
import { FeedCardComponent } from './feed-card/feed-card.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { HttpClientModule } from '@angular/common/http';

describe('FeedListComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        CommonModule,
        RouterModule,
        FormsModule,
        CardModule,
        ButtonModule,
        InputTextModule,
        InputTextareaModule,
      ],
      declarations: [FeedListComponent, FeedCardComponent],
    }).compileComponents();
  }));

  it('should create the feed-list', () => {
    const fixture = TestBed.createComponent(FeedListComponent);
    const feedList = fixture.debugElement.componentInstance;
    expect(feedList).toBeTruthy();
  });
});
