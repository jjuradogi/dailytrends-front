import { TestBed, async } from '@angular/core/testing';
import { FeedDetailComponent } from './feed-detail.component';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

describe('FeedDetailComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule, FormsModule, CommonModule, RouterTestingModule, HttpClientModule],
      declarations: [FeedDetailComponent],
    });
  }));

  it('should create the feed-edit view', () => {
    const fixture = TestBed.createComponent(FeedDetailComponent);
    const feedEdit = fixture.debugElement.componentInstance;

    expect(feedEdit).toBeTruthy();
  });
});
