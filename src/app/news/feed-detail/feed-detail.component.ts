import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Feed } from '../feed.model';
import { FeedService } from 'src/app/shared/services/feed.service';

@Component({
  selector: 'app-feed-detail',
  templateUrl: './feed-detail.component.html',
  styleUrls: ['./feed-detail.component.scss'],
})
export class FeedDetailComponent implements OnInit {
  feed: Feed;

  constructor(
    private route: ActivatedRoute,
    private feedService: FeedService,
    private location: Location
  ) {}

  ngOnInit() {
    const _id = this.route.snapshot.paramMap.get('_id');
    this.getOneFeed(_id);
  }

  getOneFeed(_id: string) {
    this.feedService.getFeedById(_id).subscribe(feed => (this.feed = feed));
  }

  goBack(): void {
    this.location.back();
  }

  deleteFeed(_id: string) {
    if (
      confirm(
        '¿Estás seguro que quieres eliminar esta noticia? No habrá posibilidad de deshacer...'
      )
    ) {
      this.feedService
        .deleteFeed(_id)
        .subscribe(feed => (feed ? this.goBack() : alert('Ha ocurrido un error')));
    }
  }
}
