import { TestBed, async } from '@angular/core/testing';
import { FeedNewComponent } from './feed-new.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';

describe('FeedNewComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule, HttpClientModule],
      declarations: [FeedNewComponent],
    }).compileComponents();
  }));

  it('should create the feed-new view', () => {
    const fixture = TestBed.createComponent(FeedNewComponent);
    const feedNew = fixture.debugElement.componentInstance;

    expect(feedNew).toBeTruthy();
  });
});
