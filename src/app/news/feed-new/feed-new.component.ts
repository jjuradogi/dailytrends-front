import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { FeedService } from 'src/app/shared/services/feed.service';

import { Feed } from '../feed.model';

@Component({
  selector: 'app-feed-new',
  templateUrl: './feed-new.component.html',
  styleUrls: ['./feed-new.component.scss'],
})
export class FeedNewComponent implements OnInit {
  feed: Feed = {
    _id: '',
    title: '',
    body: '',
    image: '',
    source: '',
    publisher: '',
  };

  constructor(private location: Location, private feedService: FeedService) {}

  ngOnInit() {}

  newFeed() {
    this.feedService
      .newFeed(this.feed)
      .subscribe(feed => (feed ? this.location.back() : alert('Ha ocurrido un error...')));
  }
}
