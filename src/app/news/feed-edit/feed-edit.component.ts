import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Feed } from '../feed.model';
import { FeedService } from 'src/app/shared/services/feed.service';

@Component({
  selector: 'app-feed-edit',
  templateUrl: './feed-edit.component.html',
  styleUrls: ['./feed-edit.component.scss'],
})
export class FeedEditComponent implements OnInit {
  feed: Feed;

  constructor(
    private route: ActivatedRoute,
    private feedService: FeedService,
    private location: Location
  ) {}
  ngOnInit() {
    const _id = this.route.snapshot.paramMap.get('_id');
    this.getOneFeed(_id);
  }

  getOneFeed(_id: string) {
    this.feedService.getFeedById(_id).subscribe(feed => (this.feed = feed));
  }

  updateFeed(_id: string, feed: Feed) {
    this.feedService
      .updateFeed(_id, feed)
      .subscribe(feed => (feed ? this.location.back() : alert('Ha ocurrido un error')));
  }
}
