import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';

import { FeedListComponent } from './feed-list/feed-list.component';
import { FeedDetailComponent } from './feed-detail/feed-detail.component';
import { FeedCardComponent } from './feed-list/feed-card/feed-card.component';
import { FeedNewComponent } from './feed-new/feed-new.component';
import { FeedEditComponent } from './feed-edit/feed-edit.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    CardModule,
    ButtonModule,
    InputTextModule,
    InputTextareaModule,
  ],
  declarations: [
    FeedListComponent,
    FeedDetailComponent,
    FeedCardComponent,
    FeedNewComponent,
    FeedEditComponent,
  ],
  exports: [],
  providers: [],
  bootstrap: [],
})
export class NewsModule {}
