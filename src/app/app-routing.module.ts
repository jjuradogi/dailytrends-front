import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeedListComponent } from './news/feed-list/feed-list.component';
import { FeedDetailComponent } from './news/feed-detail/feed-detail.component';
import { FeedNewComponent } from './news/feed-new/feed-new.component';
import { FeedEditComponent } from './news/feed-edit/feed-edit.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FeedListComponent,
  },
  {
    path: 'feed/:_id/details',
    component: FeedDetailComponent,
  },
  {
    path: 'feed/:_id/edit',
    component: FeedEditComponent,
  },
  {
    path: 'feed/new',
    component: FeedNewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
