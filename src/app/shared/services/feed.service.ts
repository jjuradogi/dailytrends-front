import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Feed } from '../../news/feed.model';

@Injectable({
  providedIn: 'root',
})
export class FeedService {
  feedUrl = 'http://localhost:3001/api';

  constructor(private http: HttpClient) {}

  getFeeds(): Observable<Feed[]> {
    return this.http.get<Feed[]>(this.feedUrl);
  }

  getFeedById(_id: string): Observable<Feed> {
    return this.http.get<Feed>(this.feedUrl + '/' + _id);
  }

  newFeed(feed: Feed): Observable<Feed> {
    delete feed._id;

    return this.http.post<Feed>(this.feedUrl, feed);
  }

  updateFeed(_id: string, feed: Feed): Observable<Feed> {
    delete feed._id;

    return this.http.put<Feed>(this.feedUrl + '/' + _id, feed);
  }

  deleteFeed(_id: string): Observable<Feed> {
    return this.http.delete<Feed>(this.feedUrl + '/' + _id);
  }
}
