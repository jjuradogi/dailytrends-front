import { Component, ViewEncapsulation } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
  title = 'Daily Trends';

  constructor(private location: Location) {}

  goBack(): void {
    this.location.back();
  }

  isRootPage(): boolean {
    return this.location.path() === '' ? false : true;
  }
}
